package com.rafael.cursomc.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.rafael.cursomc.domain.City;
import com.rafael.cursomc.repositories.CityRepository;

@Service
public class CityService {
	
	@Autowired
	private CityRepository cityRepository;
	
	public List<City> findByState(Integer stateId) {
		return cityRepository.findCitiesByStateId(stateId);
	}
}
