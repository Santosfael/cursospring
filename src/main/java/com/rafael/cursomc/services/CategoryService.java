package com.rafael.cursomc.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import com.rafael.cursomc.domain.Category;
import com.rafael.cursomc.dto.CategoryDTO;
import com.rafael.cursomc.repositories.CategoryRepository;
import com.rafael.cursomc.services.expections.DataIntegratyException;
import com.rafael.cursomc.services.expections.ObjectNotFoundException;

@Service
public class CategoryService {
	
	@Autowired
	private CategoryRepository categoryRepository;
	
	public Category find(Integer id) {
		Optional<Category> optCategory = categoryRepository.findById(id);
		
		return optCategory.orElseThrow(() -> new ObjectNotFoundException("Objeto não pode ser encontrado! Id: " + id + ", Tipo: " + Category.class.getName()));
		
	}
	
	public Category insert(Category objCategory) {
		
		objCategory.setId(null);
		
		return categoryRepository.save(objCategory);
	}
	
	
	public Category update(Category objCategory) {
		Category newObj = find(objCategory.getId());
		updateData(newObj, objCategory);
		return categoryRepository.save(newObj);
	}
	
	public void delete(Integer id) {
		find(id);
		try {
			categoryRepository.deleteById(id);
		} catch (DataIntegrityViolationException e) {
			throw new DataIntegratyException("Não é possível excluir uma categoria que possui produtos");
		}
		
	}
	
	public List<Category> findAll() {
		return categoryRepository.findAll();
	}
	
	public Page<Category> findPage(Integer page, Integer linesPerPage, String orderBy, String direction) {
		PageRequest pageRequest = PageRequest.of(page, linesPerPage, Direction.valueOf(direction), orderBy);	
		return categoryRepository.findAll(pageRequest);
	}
	
	public Category fromDTO(CategoryDTO objDTO) {
		return new Category(objDTO.getId(), objDTO.getName());
	}
	
	private void updateData(Category newObj, Category obj) {
		newObj.setName(obj.getName());
	}
}
