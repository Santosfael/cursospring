package com.rafael.cursomc.services;

import java.util.Calendar;
import java.util.Date;

import org.springframework.stereotype.Service;

import com.rafael.cursomc.domain.PaymentBoleto;

@Service
public class BoletoService {
	
	public void insertDataPaymentBoleto(PaymentBoleto payment, Date instanteOrder) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(instanteOrder);
		cal.add(Calendar.DAY_OF_MONTH, 7);
		payment.setDueDate(cal.getTime());
	}
}
