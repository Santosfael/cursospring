package com.rafael.cursomc.services;

import javax.mail.internet.MimeMessage;

import org.springframework.mail.SimpleMailMessage;

import com.rafael.cursomc.domain.Client;
import com.rafael.cursomc.domain.Order;

public interface EmailService {
	
	void sendOrderConfirmationEmail(Order order);
	
	void sendEmail(SimpleMailMessage msg);
	
	void sendOrderConfirmationHtmlEmail(Order obj);
	
	void sendHtmlEmail(MimeMessage msg);
	
	void senNewPasswordEmail(Client client, String newPass);
}
