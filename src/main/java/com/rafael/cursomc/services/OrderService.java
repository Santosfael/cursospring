package com.rafael.cursomc.services;

import java.util.Date;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.rafael.cursomc.domain.Client;
import com.rafael.cursomc.domain.Order;
import com.rafael.cursomc.domain.OrderItem;
import com.rafael.cursomc.domain.PaymentBoleto;
import com.rafael.cursomc.domain.enuns.PaymentState;
import com.rafael.cursomc.repositories.OrderItemRepository;
import com.rafael.cursomc.repositories.OrderRepository;
import com.rafael.cursomc.repositories.PaymentRepository;
import com.rafael.cursomc.security.UserSS;
import com.rafael.cursomc.services.expections.AuthorizationException;
import com.rafael.cursomc.services.expections.ObjectNotFoundException;

@Service
public class OrderService {
	
	@Autowired
	private OrderRepository orderRepository;
	
	@Autowired
	private BoletoService boletoService;
	
	@Autowired
	private PaymentRepository paymentRepository;
	
	@Autowired
	private ProductService productService;
	
	@Autowired
	private OrderItemRepository orderItemRepository;
	
	@Autowired 
	private ClientService clientService;
	
	@Autowired
	private EmailService emailService;
	
	public Order find(Integer id) {
		Optional<Order> optOrder = orderRepository.findById(id);
		
		return optOrder.orElseThrow(() -> new ObjectNotFoundException("Cliente não pode ser encontrado! Id: " + id + ", Tipo: " + Order.class.getName()));
	}
	
	@Transactional
	public Order insert(Order orderObj) {
		orderObj.setId(null);
		orderObj.setIntante(new Date());
		orderObj.setClient(clientService.find(orderObj.getClient().getId()));
		orderObj.getPayment().setPaymentState(PaymentState.PENDENT);
		orderObj.getPayment().setOrder(orderObj);
		if(orderObj.getPayment() instanceof PaymentBoleto) {
			PaymentBoleto payment = (PaymentBoleto) orderObj.getPayment();
			boletoService.insertDataPaymentBoleto(payment, orderObj.getInstante());
		}
		
		orderObj = orderRepository.save(orderObj); 
		paymentRepository.save(orderObj.getPayment());
		
		for (OrderItem ip : orderObj.getItens()) {
			ip.setDiscount(0.0);
			ip.setProduct(productService.find(ip.getProduct().getId()));
			ip.setPrice(productService.find(ip.getProduct().getId()).getPrice());
			ip.setOrder(orderObj);
		}
		
		orderItemRepository.saveAll(orderObj.getItens());
		emailService.sendOrderConfirmationHtmlEmail(orderObj);
		return orderObj;
	}
	
	public Page<Order> findPage(Integer page, Integer linesPerPage, String orderBy, String direction) {
		UserSS user = UserService.authenticated();
		if(user == null) {
			throw new AuthorizationException("Acesso negado");
		}
		PageRequest pageRequest = PageRequest.of(page, linesPerPage, Direction.valueOf(direction), orderBy);
		Client client = clientService.find(user.getId());
		
		return orderRepository.findByClient(client, pageRequest);
	}
}
