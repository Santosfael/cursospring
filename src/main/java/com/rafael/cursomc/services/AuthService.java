package com.rafael.cursomc.services;

import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.rafael.cursomc.domain.Client;
import com.rafael.cursomc.repositories.ClientRepository;
import com.rafael.cursomc.services.expections.ObjectNotFoundException;

@Service
public class AuthService {
	
	@Autowired
	private ClientRepository clientRepository;
	
	@Autowired
	private BCryptPasswordEncoder pe;
	
	@Autowired
	private EmailService emailService;
	
	private Random rand = new Random();
	
	public void sendNewPassword(String email) {
		Client client = clientRepository.findByEmail(email);
		if(client == null) {
			throw new ObjectNotFoundException("E-mail não encontrado");
		}
		
		String newPass = newPassword();
		client.setPassword(pe.encode(newPass));
		
		clientRepository.save(client);
		emailService.senNewPasswordEmail(client, newPass);
	}

	private String newPassword() {
		char[] vet = new char[10];
		for(int i =0; i < 10; i++) {
			vet[i] = randomChar();
		}
		
		return new String(vet);
	}

	private char randomChar() {
		int opt = rand.nextInt(3);
		if(opt == 0) { //gerar um digito
			return (char) (rand.nextInt(10) + 48);
		} else if (opt == 1) {//gerar uma letra maiuscula
			return (char) (rand.nextInt(26) + 65);
		} else { // gerar uma letra minuscula;
			return (char) (rand.nextInt(26) + 97);
		}
	}
}
