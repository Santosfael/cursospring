package com.rafael.cursomc.services;

import java.awt.image.BufferedImage;
import java.net.URI;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.rafael.cursomc.domain.Address;
import com.rafael.cursomc.domain.City;
import com.rafael.cursomc.domain.Client;
import com.rafael.cursomc.domain.enuns.ClientType;
import com.rafael.cursomc.domain.enuns.Profile;
import com.rafael.cursomc.dto.ClientDTO;
import com.rafael.cursomc.dto.ClientNewDTO;
import com.rafael.cursomc.repositories.AddressRepository;
import com.rafael.cursomc.repositories.ClientRepository;
import com.rafael.cursomc.security.UserSS;
import com.rafael.cursomc.services.expections.AuthorizationException;
import com.rafael.cursomc.services.expections.DataIntegratyException;
import com.rafael.cursomc.services.expections.ObjectNotFoundException;

@Service
public class ClientService {
	
	@Autowired
	private BCryptPasswordEncoder pe;
	
	@Autowired
	private ClientRepository clientRepository;
	
	@Autowired
	private AddressRepository addressRepository;
	
	@Autowired
	private S3Service s3Service;
	
	@Autowired
	private ImageService imageService;
	
	@Value("${img.prefix.client.profile}")
	private String prefix;
	
	@Value("${img.profile.size}")
	private Integer size;
	
	public Client find(Integer id) {
		UserSS user = UserService.authenticated();
		if(user == null || !user.hasRole(Profile.ADMIN) && !id.equals(user.getId())) {
			throw new AuthorizationException("Acesso negado");
		}
		Optional<Client> optClient = clientRepository.findById(id);
		
		return optClient.orElseThrow(() -> new ObjectNotFoundException("Cliente não pode ser encontrado! Id: " + id + ", Tipo: " + Client.class.getName()));
	}
	
	@Transactional
	public Client insert(Client obj) {
		obj.setId(null);
		obj = clientRepository.save(obj);
		addressRepository.saveAll(obj.getAdresses());
		return obj;
	}
	 
	public Client update(Client objClient) {
		Client newObj = find(objClient.getId());
		updateData(newObj, objClient);
		return clientRepository.save(newObj);
	}
	
	public void delete(Integer id) {
		find(id);
		try {
			clientRepository.deleteById(id);
		} catch (DataIntegrityViolationException e) {
			throw new DataIntegratyException("Não é possível excluir o cliente pois o cliente tem pedidos");
		}
		
	}
	
	public List<Client> findAll() {
		return clientRepository.findAll();
	}
	
	public Client findByEmail(String email) {
		UserSS user = UserService.authenticated();
		if(user == null || !user.hasRole(Profile.ADMIN) && !email.equals(user.getUsername())) {
			throw new AuthorizationException("Acesso negado");
		}
		
		Client obj = clientRepository.findByEmail(email);
		
		if(obj == null) {
			throw new ObjectNotFoundException("Objeto não encontrado! Id: " + user.getId() + "Tipo: " + Client.class.getName());
		}
		
		return obj;
	}
	
	public Page<Client> findPage(Integer page, Integer linesPerPage, String orderBy, String direction) {
		PageRequest pageRequest = PageRequest.of(page, linesPerPage, Direction.valueOf(direction), orderBy);
		
		return clientRepository.findAll(pageRequest);
	}
	
	public Client fromDTO(ClientDTO objDTO) {
		return new Client(objDTO.getId(), objDTO.getName(), objDTO.getEmail(), null, null, null);
	}
	
	public Client fromDTO(ClientNewDTO objDTO) {
		Client cli = new Client(null, objDTO.getName(), objDTO.getEmail(), objDTO.getCpfCnpj(), ClientType.toEnum(objDTO.getClientType()), pe.encode(objDTO.getPassword()));
		City city = new City(objDTO.getCityId(), null, null);
		Address address = new Address(null, objDTO.getStreet(), objDTO.getNumber(), objDTO.getComplement(), objDTO.getNeighborhood(), objDTO.getZipCode(), cli, city);
		cli.getAdresses().add(address);
		cli.getPhones().add(objDTO.getPhone1());
		if(objDTO.getPhone2() != null)
			cli.getPhones().add(objDTO.getPhone2());
		
		if(objDTO.getPhone3() != null)
			cli.getPhones().add(objDTO.getPhone3());
		
		return cli;
	}
	
	private void updateData(Client newObj, Client obj) {
		newObj.setName(obj.getName());
		newObj.setEmail(obj.getEmail());
	}
	
	public URI uploadProfilePicture(MultipartFile multipartFile) {
		UserSS user = UserService.authenticated();
		if(user == null) {
			throw new AuthorizationException("Acesso negado");
		}
		BufferedImage jpgImage = imageService.getJpgImageFromFile(multipartFile);
		jpgImage = imageService.cropSquare(jpgImage);
		jpgImage = imageService.resize(jpgImage, size);
		
		String fileName = prefix + user.getId() + ".jpg";
		
		return s3Service.uploadFile(imageService.getInputStream(jpgImage, "jpg"), fileName, "image");

	}
}
