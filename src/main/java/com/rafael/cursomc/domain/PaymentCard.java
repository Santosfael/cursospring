package com.rafael.cursomc.domain;

import javax.persistence.Entity;

import com.fasterxml.jackson.annotation.JsonTypeName;
import com.rafael.cursomc.domain.enuns.PaymentState;

@Entity
@JsonTypeName("pagamentoComCartao")
public class PaymentCard extends Payment {
	private static final long serialVersionUID = 1L;
	
	private Integer numberParcelas;
	
	public PaymentCard() {}

	public PaymentCard(Integer id, PaymentState paymentState, Order order, Integer numberParcelas) {
		super(id, paymentState, order);
		this.numberParcelas = numberParcelas;
	}

	public Integer getNumberParcelas() {
		return numberParcelas;
	}

	public void setNumberParcelas(Integer numberParcelas) {
		this.numberParcelas = numberParcelas;
	}
	
}
