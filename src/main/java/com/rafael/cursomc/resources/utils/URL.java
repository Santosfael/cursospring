package com.rafael.cursomc.resources.utils;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class URL {
	
	public static String decodeParam(String s) {
		try {
			return URLDecoder.decode(s, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			return "";
		}
	}
	
	public static List<Integer> decodeIntList(String s) {
		/*
		 * String[] vetCategories = s.split(",");
			List<Integer> listCategories = new ArrayList<>();
			
			for (int i = 0; i < vetCategories.length; i++) {
				listCategories.add(Integer.parseInt(vetCategories[i]));
			}
			
			return listCategories;
		*/
		return Arrays.asList(s.split(",")).stream().map(x -> Integer.parseInt(x)).collect(Collectors.toList());
		
		
	}
}
