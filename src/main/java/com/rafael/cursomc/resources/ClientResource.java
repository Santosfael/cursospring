package com.rafael.cursomc.resources;

import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.rafael.cursomc.domain.Client;
import com.rafael.cursomc.dto.ClientDTO;
import com.rafael.cursomc.dto.ClientNewDTO;
import com.rafael.cursomc.services.ClientService;

@RestController
@RequestMapping(value="clients")
public class ClientResource {
	
	@Autowired
	private ClientService clientService;
	
	@RequestMapping(value="/{id}", method = RequestMethod.GET)
	public ResponseEntity<Client> find(@PathVariable Integer id) {
		Client clientObj =  clientService.find(id);
		
		return ResponseEntity.ok().body(clientObj);
	}
	
	@RequestMapping(value="/email", method = RequestMethod.GET)
	public ResponseEntity<Client> find(@RequestParam(value = "value") String email) {
		Client obj = clientService.findByEmail(email);
		return ResponseEntity.ok().body(obj);
	}
	
	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<Void> insert(@Valid @RequestBody ClientNewDTO objClientDTO) {
		Client objClient = clientService.fromDTO(objClientDTO);
		objClient = clientService.insert(objClient);
		URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(objClient .getId()).toUri();
		
		return ResponseEntity.created(uri).build();
	}
	
	@RequestMapping(value="/{id}", method = RequestMethod.PUT)
	public ResponseEntity<Void> update(@Valid @RequestBody ClientDTO objClientDTO, @PathVariable Integer id) {
		Client objClient = clientService.fromDTO(objClientDTO);
		objClient.setId(id);
		objClient = clientService.update(objClient);
		
		return ResponseEntity.noContent().build();
	}
	
	@PreAuthorize("hasAnyRole('ADMIN')")
	@RequestMapping(value="/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Void> delete(@PathVariable Integer id) {
		clientService.delete(id);
		return ResponseEntity.noContent().build();
	}
	
	@PreAuthorize("hasAnyRole('ADMIN')")
	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<List<ClientDTO>> findAll() {
		List<Client> listClient = clientService.findAll();
		List<ClientDTO> listClientDTO = listClient.stream().map(obj -> new ClientDTO(obj)).collect(Collectors.toList());
		
		return ResponseEntity.ok().body(listClientDTO);
	}
	
	@PreAuthorize("hasAnyRole('ADMIN')")
	@RequestMapping(value="/page", method = RequestMethod.GET)
	public ResponseEntity<Page<ClientDTO>> findPage(
			@RequestParam(value="page", defaultValue = "0") Integer page, 
			@RequestParam(value="linesPerPage", defaultValue = "24") Integer linesPerPage, 
			@RequestParam(value="orderBy", defaultValue = "name") String orderBy, 
			@RequestParam(value="direction", defaultValue = "ASC") String direction) {
		Page<Client> listClient = clientService.findPage(page, linesPerPage, orderBy, direction);
		Page<ClientDTO> listClientDTO = listClient.map(obj -> new ClientDTO(obj));
		
		return ResponseEntity.ok().body(listClientDTO);
	}
	
	@RequestMapping(value = "/picture", method = RequestMethod.POST)
	public ResponseEntity<Void> uploadProfilePicture(@RequestParam(name="file") MultipartFile file ) {
		
		URI uri = clientService.uploadProfilePicture(file);
		return ResponseEntity.created(uri).build();
	}

}
